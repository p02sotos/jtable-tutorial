/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.table.model;

import com.model.entity.Persona;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class TableModel extends AbstractTableModel {

    private Persona persona;
    private List<Persona> listaPersonas;
    Field[] campos;

    public TableModel(ArrayList lista) {
        this.listaPersonas = lista;
        campos = Persona.class.getDeclaredFields();
    }

    @Override
    public int getRowCount() {
        return listaPersonas.size();
    }

    @Override
    public int getColumnCount() {
        return campos.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        persona = listaPersonas.get(rowIndex); //recuperamos el objeto persona
        switch (columnIndex) {
            case 0:
                return persona.getNombre();
            case 1:
                return persona.getEdad();
        }
        return null;
    }    
    @Override
    public String getColumnName(int column) {
        return campos[column].getName();

    }

    public void addPersona(Persona persona) {
        listaPersonas.add(persona);
        fireTableDataChanged();
    }

    public void addPersona(Persona persona, int row) {
        listaPersonas.add(row, persona);
        fireTableDataChanged();
    }

    public void deletePersona(Persona persona) {
        listaPersonas.remove(persona);
        fireTableDataChanged();
    }

    public void deletePersona(int row) {
        listaPersonas.remove(row);
        fireTableDataChanged();
    }

    
}